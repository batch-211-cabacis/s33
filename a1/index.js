console.log('Hello Gel');


fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json))

// Getting all TO DO LIST ITEM

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => {
	let list = json.map((todo => {
		return todo.title;
	}))
	console.log(list);
})

// GET all specific to do list item
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`));



// CREATE a todo list item using POST method

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST'
	headers: {
		'Content-Type': 'application.json'
	}
	body: JSON.stringify ({
		title: 'Created To Do list item',
		completed: false,
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

// UPDATE To Do list item using PUT method
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	}
	body: JSON.stringify({
		title: 'Updated to do list item',
		description: 'To update my to do list with a different data structure',
		status: 'pending',
		dateCompleted: 'pending',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

// UPDATING a To do list item using PATCH method
fetchfetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	}
	body: JSON.stringify({
		status: 'complete',
		dateCompleted: '07/09/21'
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

// DELETE a to do list item


fetch('https://jsonplaceholder.typicode.com/todos/1' , {
		method: 'DELETE',
	})
	.then((response) => response.json())
	.then((json) => console.log(json));

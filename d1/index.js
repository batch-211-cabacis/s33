// Anatomy of Client Request
/*
	http methods
	URI endpoint
	header
	body
*/

// console.log('Hello World');

// JAVASCRIPT SYNCHRONOUS AND ASYnCHRONOUS
/*
	Synchronous 
	- codes runs in sequence. This means that each operation must wait for the previous one to complete before executing.

	Asynchronous 
	- codes runs in parallel. This means that an operation can occur while another one is still being processed
	- code execution is often preferrable in situations where execution can be blocked. Some examples of this are netwrok requests, long-running calculations, file system oeprations, etc. Using asynchronous code in the browser ensures the page remians response and the user experience is mostly unaffected. 

	*by default JavaScript is synchronous/sequence
*/

console.log("Hello World");
// // console.log("Hello again");
// // for(let i = 0; i <= 10; i++){
// // 	console.log(i)
// // }
// console.log("Hello its me");



// 	- API stands for 'Application Programming Interface'
// 	- an application programming interface is a particular set of codes that allows software programs to communicate with each other
// 	- an API is the interface through which you access somone else's code or through which someone else's code accesses yours.

// 	EXAMPLE:
// 		Google APIs
// 			https://developers.google.com/identity/sign-in/web/sign-in
// 		Youtube API
// 			https://developers.google.com/youtube/iframe_api_reference



// // FETCH API


// console.log(fetch('https://jsonplaceholder.typicode.com/posts'))
/*
	- A 'promise' is an object that represents the eventual completion or failure of an asynchronous function and it's resulting value
	- it is in ine of these three state:
		1. Pending - initial state, neither fulfilled nor rejected
		2. Fullfilled - operation was successfully completed
		3. Rejected - operation failed
*/

/*
	- by using the .then method we can now check for the status of the promise.
	- the "fetch" wmethod will return a "promise" that resolves to be a "respoinse" object
	- the ".then" method captures the "response" object and returns another "promise" which will eventually be a "resolved" or "rejected"
	SYNTAX
		fetch('url')
		.then((response) => {})
// */
fetch('https://jsonplaceholder.typicode.com/posts')
.then(response => console.log(response.status))

fetch('https://jsonplaceholder.typicode.com/posts')

// // use the "json" method from the "response" object to convert the data retrieved into JSOn format to be used in our application

.then((response) => response.json())


// // print the converted JSON value from the "fetch" request
// // using multiple ".then" method to create a promise chain

.then((json) => console.log(json))

/*
	- The "async" and "await" keywords is another approach that can be used to achieve asynchronous codes
	- used in functions to indicate which portuions of code should be waited
	- creates an asynchronous function
*/


// async function fetchData (){
// 	// waits for the 'fetch' method to complete then stores the value in the 'result' variable
// 	let result = await fetch('https://jsonplaceholder.typicode.com/posts')
// 	// result returned by fetch is returned as a promise
// 	console.log(result)
// 	// the return response is an object
// 	console.log(typeof result)
// 	// cannot access the content of the response by directly accessing its body property
// 	console.log(result.body)
// 	let json = await result.json();
// 	console.log(json)
// };
// fetchData()


// Getting a Specific pots
/*
	- retrieves a specific post following the REST API (retrieve, /posts/id:, GET)
// */

fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response) => response.json())
.then((json) => console.log(json))

// /*
	// Postman
	// 	url:https://jsonplaceholder.typicode.com/posts/1
	// 	method: GET
	
// */
// /*
// 	CREATING A POST
// 	SYNTAX
// 		fetch('URL', options)
// 		.then((response) => {})
// 		.then((response) => {})
// */

// // Creates a new post following the REST API (create, /post/:id, post)

fetch('https://jsonplaceholder.typicode.com/posts', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'New post',
		body: 'Hello World',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

/*
	POSTMAN
	url: https://jsonplaceholder.typicode.com/posts
	method: POST
	body: raw + JSON
	{
		"title": "My first blog",
		"body": "Hello World",
		"userId": 1
	}
*/

/*
	UPDATING A POST
	- updates a specific post following the REST API (update, /posts/:id, PUT)
*/
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: 'Updated post',
		body: 'Hello Again',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

/*
	POSTMAN
		url: https://jsonplaceholder.typicode.com/posts/1
		method: PUT
		body: raw + json
		{
			"title": "My first revised Blog post",
			"body": "Hello there! I revised this a bit",
			"userId": 1
		}
*/

// Update a post using PATCH method
/*
	- updates a specific post following the REST API (update, /posts/:id, PATCH)
	- the differences between PUT and PATCH is the number of properties being changed
	- PATCh is used to update the parts
	- PUT is used to update a sthe whole document
*/

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Corrected post'
	})

})
.then((response) => response.json())
.then((json) => console.log(json))

/*
	POSTMAN
		url:https://jsonplaceholder.typicode.com/posts/1
		method: PATCH
		body: raw + json
		{
			"title": "This is my final title."
		}
*/

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'This is my final title.'
	})

})
.then((response) => response.json())
.then((json) => console.log(json))



// DELETING A SPECIFIC POST
/*
	- deleting a specific post the following REST API (delete, /post/:id, DELETE)
*/
fetch('https://jsonplaceholder.typicode.com/posts/1' , {
		method: 'DELETE',
	})
	.then((response) => response.json())
	.then((json) => console.log(json));

/*
	POSTMAN
		url: https://jsonplaceholder.typicode.com/posts/1
		method: DELETE
*/


// Filtering the POST
/*
	- the data can be filtered by sending the userId along woth the URL
	- information sent via URL can be done adding the question mark symbol (?)
	SYNTAX
		Individual Parameters: 
			'url?parameterName=value'
		Multiple Parameters:
			'url?paramA=valueA&paramB=valueB'
*/

fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then((response) => response.json())
.then((json) => console.log(json))

fetch('https://jsonplaceholder.typicode.com/posts?userId=1&userId=2&userId=3')
.then((response) => response.json())
.then((json) => console.log(json))


// RETRIEVE COMMENTS OF A SPECIFIC POST
/*
	- retrieving comment for a specific post following the REST API (retrieve, /posts/:id, GET)
*/

fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then((response) => response.json())
.then((json) => console.log(json))